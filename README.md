# NAME

Stanford::FlexTest - Allow testing with Test::More and Nagios output

# SYNOPSIS

    use strict;
    use warnings;

    use Stanford::FlexTest::Collection;
    use Stanford::FlexTest::Nagios;

    my $flextests = Stanford::FlexTest::Collection->new();

    # Put in Test::More mode.
    $flextests->ok_mode(1);

    my ($result, $fmsg);

    # We surround with an eval to trap any errors.
    my $number_of_tests = 0;
    eval {
        # Do a test and add to the FlexTest::Collection object.
        $result = (1+2 == 2+1);
        $fmsg   = 'addition is commutative';
        $flextests->add($result, $fmsg);
        ++$number_of_tests;

        # Do another test and add to the FlexTest::Collection object.
        $result = (1+0 == 1);
        $fmsg   = 'addition identity works';
        $flextests->add($result, $fmsg);
        ++$number_of_tests;

        # Tell Test::More we are done testing and the number of tests we
        # ran.
        done_testing($number_of_tests);
    } ;
    my $aterror = $@;

    # If desired, you can now send the results of this testing to Nagios.
    # If any tests failed this counts as a CRITICAL Nagios failure.
    my $nagios_flextester = Stanford::FlexTest::Nagios->new(
        nagios_servers => ["nagios1.example.com", "nagios2.example.com"],
        verbose        => 1,
        hostname       => "server-being-tested.example.com",
    );
    $nagios_flextester->process_flextest_results("addition tests", $flextests, $aterror);
