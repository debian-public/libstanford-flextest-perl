package Stanford::FlexTest;

use strict;
use warnings;
use autodie;

## no critic (Documentation::RequirePodAtEnd);

=head1 NAME

Stanford::FlexTest - Allow testing with Test::More and Nagios output

=cut

# Make this a Moose class
use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;
use Test::More;

has ok_mode => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    default  => 0,
);

has status => (
    is       => 'rw',
    isa      => enum([-1, 0, 1]),
    required => 1,
    default  => 0,
);

has msg => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
    default  => 'no message defined',
);

=head2 Methods

=over

=item $self->passed

Returns 1 if the test passed, 0 otherwise.

=cut

sub passed {
    my ($self) = shift;
    if ($self->status == 1) {
        return 1;
    } else {
        return 0;
    }
}

=item $self->failed

Returns 1 if the test failed, 0 otherwise.

=cut

sub failed {
    my ($self) = shift;
    if ($self->status == 0) {
        return 1;
    } else {
        return 0;
    }
}

=item $self->set_pass

Sets status to 1.

=cut

sub set_pass {
    my ($self) = shift;
    $self->status(1);
    return;
}

=item $self->set_fail

Sets status to 0.

=cut

sub set_fail {
    my ($self) = shift;
    $self->status(0);
    return;
}

=item $self->set_skip

Sets status to -1.

=cut

sub set_skip {
    my ($self) = shift;
    $self->status(-1);
    return;
}

=item $self->myok

Call the ok function of Test::More

=cut

sub myok {
    my ($self) = shift;
    return ok($self->status, $self->msg);
}

1;

=back

=cut

__END__
