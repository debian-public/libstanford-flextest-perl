package Stanford::FlexTest::Collection;

use strict;
use warnings;
use autodie;

## no critic (Subroutines::RequireArgUnpacking);
## no critic (Documentation::RequirePodAtEnd);

=head1 NAME

Stanford::FlexTest::Collection - Collections of Stanford::FlexTest objects

=cut

# Make this a Moose class
use Moose;
use namespace::autoclean;

use Stanford::FlexTest;
use Data::Dumper;

has ok_mode => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    default  => 0,
);

has tests => (
    is       => 'rw',
    isa      => 'ArrayRef[Stanford::FlexTest]',
    required => 1,
    default  => sub { [] },
);

has skip_on_first_fail => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    default  => 0,
);

# Set this to 1 to indicate that all subsequent tests are to be skipped.
has skipping => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    default  => 0,
);

=head2 Methods

=over

=item $self->add_flextest

Add a Stanford::FlexTest object to the collection.

=cut

sub add_flextest {
    my ($self)     = shift;
    my ($flextest) = @_;

    my $new_flextests = [@{ $self->tests() }, $flextest];
    $self->tests($new_flextests);

    return $self->tests();
}

=item $self->add

Given the boolean $result and a message, create a new Stanford::FlexTest
object and add it to this collection.

=cut

sub add {
    my ($self) = shift;
    my ($result, $msg) = @_;

    my $flextest = Stanford::FlexTest->new(msg => $msg);

    if (!$self->in_skip_mode()) {
        if ($result) {
            $flextest->set_pass();
        } else {
            $flextest->set_fail();
            if ($self->skip_on_first_fail) {
                $self->set_skip_mode();
            }
        }
    } else {
        # Skipping
        $flextest->set_skip();
    }

    $self->add_flextest($flextest);
    if ($self->ok_mode) {
        $flextest->myok();
    }

    return;
}

=item $self->in_skip_mode

Returns true if in skip mode.

=cut

sub in_skip_mode {
    my ($self) = shift;
    return $self->skipping();
}

=item $self->set_skip_mode

Put this collection in skip mode.

=cut

sub set_skip_mode {
    my ($self) = shift;
    $self->skipping(1);
    return $self->skipping();
}

=item $self->count

Return the number of Stanford::FlexTests in this collection.

=cut

sub count {
    my ($self) = shift;
    return scalar(@{ $self->tests() });
}

=item $self->last_flextest

Return the last Stanford::FlexTest in this collection.

=cut

sub last_flextest {
    my ($self) = shift;
    my @flextests = @{ $self->tests() };
    return $flextests[scalar(@flextests) - 1];
}

=item $self->first_failure()

Return the first Stanford::FlexTest in this collection that has
"failed" status. If there are none such, return undef.

=cut

sub first_failure {
    my ($self) = shift;
    my @flextests = @{ $self->tests() };

    foreach my $flextest (@flextests) {
        if ($flextest->failed()) {
            return $flextest;
        }
    }

    return;
}

1;

=back

=cut

__END__
