package Stanford::FlexTest::Nagios;

use strict;
use warnings;
use autodie;

## no critic (Documentation::RequirePodAtEnd);
## no critic (InputOutput::RequireCheckedSyscalls);

=head1 NAME

Stanford::FlexTest::Nagios - Support for sending passive alerts to a Nagios server

=cut

## no critic (RequireArgUnpacking);

# Make this a Moose class
use Moose;
use namespace::autoclean;

use Carp;
use IPC::Run qw/ run timeout /;

has verbose => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    default  => 0,
);

has hostname => (
    is       => 'rw',
    isa      => 'Str',
    required => 1,
    default  => 'unknown',
);

has nagios_servers => (
    is       => 'rw',
    isa      => 'ArrayRef[Str]',
    required => 1,
    default  => sub { [] },
);

=head2 Methods

=over

=item $self->exit_with_error

Print a message and exit with a positive exit code.

=cut

sub exit_with_error {
    my ($msg) = @_;
    print "error: $msg.\n";
    exit 1;
}

=item $self->progress

If in verbose mode print a message.

=cut

sub progress {
    my ($self) = shift;

    my ($msg) = @_;
    if ($self->verbose) {
        print $msg . "\n";
    }
    return;
}

=item $self->process_flextest_results

Send a Nagios passive message based on a Stanford::FlexTest::Collection object.

=cut

sub process_flextest_results {
    my ($self) = shift;

    my ($nagios_test_name, $flextests, $aterror) = @_;

    if ($aterror) {
        $self->send_unreachable_passive_to_nagios_servers($nagios_test_name);
        my $msg = "error with '$nagios_test_name': $aterror";
        $self->exit_with_error($msg);
    } else {
        $self->send_nagios_result($flextests, $nagios_test_name);
    }

    return;
}

=item $self->send_nagios_result

Send a Nagios passive message based on the last flextest in a
Stanford::FlexTest::Collection object.

=cut

sub send_nagios_result {
    my ($self) = shift;

    my ($flextests, $nagios_test_name) = @_;

    # Get the first failed flextest in $flextests.
    # If there is one, send a CRITICAL to the Nagios server.
    my $first_failed_flextest = $flextests->first_failure();
    my $msg;
    if (defined($first_failed_flextest)) {
        my $detail_message = $first_failed_flextest->msg();
        $msg = "2;CRITICAL - '$detail_message' failed";
    } else {
        $msg = '0;OK';
    }

    $self->send_passive_to_nagios_servers($nagios_test_name, $msg);
    return;
}

=item $self->send_passive_to_nagios_servers

Send a Nagios passive message.

=cut

sub send_passive_to_nagios_servers {
    my ($self) = shift;

    my ($nagios_test_name, $msg) = @_;

    if (!$nagios_test_name) {
        croak 'missing required Nagios test name';
    }

    my $hostname = $self->hostname;

    my $psv_msg = "$hostname;$nagios_test_name;$msg";
    $self->progress("passive message: $psv_msg");

    my $out;
    my @nagios_servers = @{ $self->nagios_servers };

    foreach my $nagios_server (@nagios_servers) {
        #<<<  do not let perltidy touch this
        my @command = (
            '/usr/sbin/send_nsca',
            '-d', q{;},
            '-H', $nagios_server,
            );
        #>>>
        IPC::Run::run \@command, \$psv_msg, \$out;
        my $possible_error = $?;
        if ($possible_error != 0) {
            my $cmd_string = join(q{ }, @command);
            carp qq{error running '$cmd_string': $out};
        }
        $self->progress("send_ncsa command to $nagios_server output: $out");
    }

    return;
}

=item $self->send_unreachable_passive_to_nagios_servers

Send a Nagios passive message indicating an UNKNOWN result.

=cut

sub send_unreachable_passive_to_nagios_servers {
    my ($self) = shift;

    my ($nagios_test_name) = @_;

    my $msg = '3;UNKNOWN - test failed to run properly';
    return $self->send_passive_to_nagios_servers($nagios_test_name, $msg);
}

1;

=back

=cut

__END__
